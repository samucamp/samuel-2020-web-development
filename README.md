# SAMUEL 2020 - Web Development

Websites, web apps, templates e ferramentas relacionadas

<br>

# Conteúdos

1. [Webpack Module Bundler](#webpack)
2. [SPA vs MPA](#spavsmpa)
3. [Referências](#ref)

<br>

# Webpack Module Bundler

<a name="webpack"></a>

Uma das opções para instalar o webpack no seu projeto, é através do Node Package Manager (NPM). Para baixar e entender o NPM, verificar em [SAMUEL 2020 - JavaScript e NodeJS](https://gitlab.com/samucamp/samuel-2020-javascript-node)

Instalando webpack e webpack-dev-server pelo NPM como devDependencies

```bash
npm install -D webpack
npm install -D webpack-cli
npm install -D webpack-dev-server
```

Para utilizar o webpack, adicione um NPM Script com comando webpack. E execute o comando com npm run build. Por padrão webpack gera o bundle em ./dist/main.js a partir de ./src/index.js

```json
  "scripts": {
    "build": "webpack"
  },
```

```bash
npm run build
```

Para verificar no browser, deve alterar o caminho do script no body do html

```html
<body>
  <script src="dist/main.js"></script>
</body>
```

Dessa forma o 'mode' não está configurado, e por padrão será utilizado como 'production'

Para definir o 'mode' como 'development' basta adicionar a flag --mode development. O tamanho do arquivo gerado será muito menor, e será mais conveniente na fase de desenvolvimento.

```json
  "scripts": {
    "build": "webpack --mode development"
  },
```

```bash
npm run build
```

Para obter uma página localhost atualizando cada vez que o programa é alterado, basta utilizar o webpack-dev-server, adicionando script e rodando o comando abaixo:

```json
  "scripts": {
    "build": "webpack --mode production",
    "start:dev":"webpack-dev-server --mode development"
  },
```

```bash
npm run start:dev
```

O bundle gerado permanence em memória com nome main.js, não será criado a pasta dist. Para acessar o script via html, deve alterar o caminho do script no body do html

```html
<body>
  <script src="main.js"></script>
</body>
```

<br>

## Configs, loaders e plugins do Webpack

Um exemplo de template que pode ser aplicado em vários projetos está em [
SAMUEL 2020 - Web Development/webpack](https://gitlab.com/samucamp/samuel-2020-web-development/-/blob/master/webpack).

A estrutura do app na fase de desenvolvimento se encontra em webpack/src, e o resultado pos processamento com webpack em webpack/build.

Foi utilizado o código de configuração abaixo no arquivo webpack.config.js

```javascript
const webpack = require("webpack");
const path = require("path");
const modoDev = process.env.NODE_ENV !== "production";
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: modoDev ? "development" : "production",
  entry: "./src/main.js",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "public"),
  },
  devServer: {
    contentBase: "./public",
    port: 9000,
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style.css",
    }),
    new CopyWebpackPlugin([
      {
        context: "src/",
        from: "**/*.html",
      },
      {
        context: "src/",
        from: "assets/images/**/*",
      },
    ]),
    new CleanWebpackPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.s?[ac]ss$/,
        include: path.resolve(__dirname, "src/assets/styles"),
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        include: path.resolve(__dirname, "src/assets/images"),
        use: ["file-loader"],
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)$/,
        include: path.resolve(__dirname, "src/assets/fonts"),
        use: ["file-loader"],
      },
      {
        test: /\.[jt]s[x]?$/,
        include: path.resolve(__dirname, "src"),
        use: ["babel-loader"],
      },
      {
        test: /\.txt$/,
        include: path.resolve(__dirname, "src"),
        use: ["raw-loader"],
      },
      {
        test: /\.(csv|tsv)$/,
        include: path.resolve(__dirname, "src"),
        use: ["csv-loader"],
      },
      {
        test: /\.xml$/,
        include: path.resolve(__dirname, "src"),
        use: ["xml-loader"],
      },
    ],
  },
};
```

O arquivo package.json com as dependencias utilizadas:

```json
{
  "name": "template-webpack",
  "version": "1.0.0",
  "description": "Webpack Template",
  "private": true,
  "scripts": {
    "dev": "cross-env NODE_ENV=development webpack",
    "start": "webpack-dev-server",
    "build": "cross-env NODE_ENV=production webpack"
  },
  "keywords": [],
  "author": "Samucamp",
  "license": "ISC",
  "devDependencies": {
    "@babel/core": "^7.9.0",
    "@babel/preset-env": "^7.9.5",
    "babel-loader": "^8.1.0",
    "clean-webpack-plugin": "^3.0.0",
    "copy-webpack-plugin": "^5.1.1",
    "cross-env": "^7.0.2",
    "css-loader": "^3.5.2",
    "csv-loader": "^3.0.2",
    "file-loader": "^6.0.0",
    "mini-css-extract-plugin": "^0.9.0",
    "node-sass": "^4.13.1",
    "optimize-css-assets-webpack-plugin": "^5.0.3",
    "raw-loader": "^4.0.1",
    "sass-loader": "^8.0.2",
    "terser-webpack-plugin": "^2.3.5",
    "webpack": "^4.42.1",
    "webpack-cli": "^3.3.11",
    "webpack-dev-server": "^3.10.3",
    "xml-loader": "^1.2.1"
  }
}
```

Para mais informações, verificar em [webpack](https://webpack.js.org/)

<br>

# SPA vs MPA

<a name="spavsmpa"></a>

## SPA - Single Page Application

PROS

- Highly reactive
- Decoupled frontend

CONS

- SEO is challenging
- JavaScript strictly required

OTHERS

- Tends to favor modern browser

## MPA - Multi Page Application

PROS

- SEO is simple
- Loads of existing frameworks, solutions and best practices

CONS

- Slower, always needs to load pages
- Tightly coupled frontend and backend

OTHERS

- Tends to support more legacy browsers

<br>

# Referências

<a name="ref"></a>

> https://webpack.js.org/

> https://www.udemy.com/course/curso-web/
