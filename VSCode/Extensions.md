# VS Code Extensions

* Beautify
* Bracket Pair Colorizer
* Code Runner
* Color Highlight
* DotENV
* Dracula Official
* ESLint
* GitLens - Git supercharged
* Live Server
* Markdown All in One
* Markdown Preview Enhanced
* Material Icon Theme
* Polacode
* Python
* Python Preview
* Todo Tree