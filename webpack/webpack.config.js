const webpack = require('webpack')
const path = require('path')
// modoDev utilizando pacote cross-env para funcionar multiplataforma */
const modoDev = process.env.NODE_ENV !== 'production'

/* Plugins utilizados */
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin') // build apenas arquivos novos, deleta antigos
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin') 
// uglifyjs-webpack-plugin => alternativo ao terser, mas contém vulnerabilidade e incompatibilidades

/* Variaveis, build folder, server path */
const entryFilename = './src/main.js'
const outputFilename = 'bundle.js'
const buildFilename = 'build'
const buildPath = path.resolve(__dirname, buildFilename)
const templateFilename = 'index.html'
const templatePath = path.join(__dirname, 'src', templateFilename)

module.exports = {
    mode: modoDev ? 'development' : 'production',
    /* entry -> arquivo que o webpack inicia a busca dos modulos */
    entry: entryFilename,
    output: {
        filename: outputFilename,
        path: buildPath
    },
    devServer: {
        // serve os arquivos que estão em public/dist/build
        contentBase: buildFilename,
        port: 9000
    },
    // otimização de javascript e css minificando 
    // minimizers só roda em --mode production
    optimization: {
        minimizer: [
            // UglifyJsPlugin é um plugin alternativo ao compressor nativo do webpack
            //new UglifyJsPlugin({
            //    cache: true,
            //    parallel: true
            //}),
            // Terser apresentou resultado melhor em comparaçao ao UglifyJS
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    ecma: 6
                }
            }),
            // comprime o bundle css gerado pelo extract plugin
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        // sempre apaga arquivos do output (public ou dist) antes de gerar novos
        new CleanWebpackPlugin(),
        // cria arquivo .html com os links prontos style e script
        new HtmlWebpackPlugin({
            title: 'Webpack Template',
            template: templatePath,
            filename: templateFilename
        }),
        // para extrair o css (ao invés de injetado na DOM pelo style-loader) e gerar um arquivo bundle css
        new MiniCssExtractPlugin({
            filename: "style.css"
        }),
        new CopyWebpackPlugin([
            // copy html conflitante com HtmlWebpackPlugin
            //{
            // context: 'src/',
            //   from: '**/*.html',
            //},
            {
                context: 'src/',
                from: 'assets/images/**/*'
            }
        ])
    ],
    module: {
        rules: [{
            // regex para css, scss e sass
            test: /\.s?[ac]ss$/,
            exclude: /node_modules/,
            //otimização, será aplicado loader apenas na pasta correta
            include: path.resolve(__dirname, 'src/assets/styles'),
            use: [
                //MiniCssExtractPlugin é conflitante com style-loader, pois ele cria um arquivo bundle css
                MiniCssExtractPlugin.loader,
                // 'style-loader', // Adiciona CSS a DOM injetando a tag <style>
                'css-loader', // interpreta @import, url()...
                'sass-loader',
                // sass-loader requere node-sass para funcionar
            ]
        }, {
            // loader para images, fontes e outros
            test: /\.(png|svg|jpg|gif)$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src/assets/images'),
            use: ['file-loader']
        }, {
            test: /\.(ttf|otf|eot|svg|woff(2)?)$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src/assets/fonts'),
            use: ['file-loader']
        }, {
            test: /\.[jt]s[x]?$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src'),
            use: ['babel-loader']
        }, {
            test: /\.txt$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src'),
            use: ['raw-loader']
        }, {
            test: /\.(csv|tsv)$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src'),
            use: ['csv-loader']
        }, {
            test: /\.xml$/,
            exclude: /node_modules/,
            include: path.resolve(__dirname, 'src'),
            use: ['xml-loader']
        }],
    },
};