// importando outros arquivos js
import Greet from './scripts/greeting'

// importando arquivo js da pasta assets. Quando não é especificado arquivo, busca por index.js
import './assets'

const pessoa = new Greet
console.log('Hello World from index.js')
console.log(pessoa.cumprimentar("Samu") + ' (imported from greeting.js and bundled using webpack)')