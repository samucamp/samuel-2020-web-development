// Arquivos SASS
import './assets/styles/index.scss'

// Dependencias JS
import 'bootstrap'
import 'jquery'

// Personal JS
import './scripts/core/includes'

// TODO: Simple galeria project using Bootstrap, Webpack, JQuery
// FIXME: ajax includes not working, html page blank