const webpack = require('webpack')
const path = require('path')
const modoDev = process.env.NODE_ENV !== 'production'

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin')

const entryFilename = './src/index.js'
const outputFilename = 'bundle.js'
const buildFilename = 'build'
const buildPath = path.resolve(__dirname, buildFilename)
const templateFilename = 'index.html'
const templatePath = path.join(__dirname, 'src', templateFilename)

module.exports = {
    mode: modoDev ? 'development' : 'production',
    entry: entryFilename,
    output: {
        filename: outputFilename,
        path: buildPath
    },
    devServer: {
        contentBase: buildFilename,
        port: 9000
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    ecma: 6
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Webpack Template',
            template: templatePath,
            filename: templateFilename
        }),
        new MiniCssExtractPlugin({
            filename: "style.css"
        })
    ],
    module: {
        rules: [{
            test: /\.s?[ac]ss$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'sass-loader',
            ]
        }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: ['file-loader']
        }, {
            test: /\.(ttf|otf|eot|svg|woff(2)?)$/,
            use: ['file-loader']
        }, {
            test: /\.[jt]s[x]?$/,
            use: ['babel-loader']
        }],
    },
};